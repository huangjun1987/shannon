﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="XEx16ProductMaint.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ch16: Product Maintenance</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/site.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        <header class="jumbotron"><%-- image set in site.css --%></header>
        <main>
            <form id="form1" runat="server" class="form-horizontal">
                <%-- error message label and validation summary controls --%>
                <div class="row">
                    <div class="col-sm-12">
                        <asp:Label ID="lblError" runat="server"
                            EnableViewState="False" CssClass="text-danger" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server"
                            HeaderText="Please correct the following errors:"
                            ValidationGroup="Edit" CssClass="text-danger" />
                    </div>
                    <div class="col-sm-6">
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server"
                            HeaderText="Please correct the following errors:"
                            ValidationGroup="New" CssClass="text-danger" />
                    </div>
                </div>

                <%-- ListView control --%>
                <asp:ListView ID="ListView1" runat="server" DataKeyNames="ProductID" DataSourceID="SqlDataSource1" InsertItemPosition="LastItem">
                    <EditItemTemplate>
                        <div style="" class="col-sm-6">
                            <div class="list-group">
                                <div class="list-group-item  bg-halloween">
                                    Current Product
                                </div>
                                <div class="list-group-item">
                                    <table class="table ">
                                        <asp:TableRow>
                                            <asp:TableCell>ProductID:</asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="ProductIDLabel1" runat="server" Text='<%# Eval("ProductID") %>' class="form-control " />
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>Name:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' class="form-control " />
												<asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="NameTextBox" Text="*" ErrorMessage="Please enter Name!" ValidationGroup="Edit" CssClass="text-danger"></asp:RequiredFieldValidator>
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>ShortDescription:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="ShortDescriptionTextBox" runat="server" Text='<%# Bind("ShortDescription") %>' class="form-control " />
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>LongDescription:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="LongDescriptionTextBox" runat="server" Text='<%# Bind("LongDescription") %>' class="form-control" TextMode="MultiLine" Rows="10"/>
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>CategoryID:</asp:TableCell><asp:TableCell>
                                                <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="LongName" DataValueField="CategoryID" SelectedValue='<%# Bind("CategoryID") %>' ></asp:DropDownList>
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>ImageFile:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="ImageFileTextBox" runat="server" Text='<%# Bind("ImageFile") %>' class="form-control " />
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>UnitPrice:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="UnitPriceTextBox" runat="server" Text='<%# Bind("UnitPrice") %>' class="form-control " />
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>OnHand:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="OnHandTextBox" runat="server" Text='<%# Bind("OnHand") %>' class="form-control " />
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" ValidationGroup="Edit" />
                                            </asp:TableCell><asp:TableCell>
                                                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" /></asp:TableCell>
                                        </asp:TableRow>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>


                    <InsertItemTemplate>
                        <div style="" class="col-sm-6">
                            <div class="list-group">
                                <div class="list-group-item  bg-halloween">
                                    Enter New Product </div><div class="list-group-item">
                                    <table class="table ">
                                        <asp:TableRow>
                                            <asp:TableCell>ProductID:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="ProductIDTextBox" runat="server" Text='<%# Bind("ProductID") %>' class="form-control " />
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>Name:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="NameTextBox" runat="server" Text='<%# Bind("Name") %>' class="form-control " />
												<asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="NameTextBox" Text="*" ErrorMessage="Please enter Name!" ValidationGroup="New" CssClass="text-danger"></asp:RequiredFieldValidator>
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>ShortDescription:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="ShortDescriptionTextBox" runat="server" Text='<%# Bind("ShortDescription") %>' class="form-control " />
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>LongDescription:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="LongDescriptionTextBox" runat="server" Text='<%# Bind("LongDescription") %>' class="form-control" TextMode="MultiLine" Rows="10"/>
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>CategoryID:</asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="LongName" DataValueField="CategoryID" SelectedValue='<%# Bind("CategoryID") %>' ></asp:DropDownList>
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>ImageFile:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="ImageFileTextBox" runat="server" Text='<%# Bind("ImageFile") %>' class="form-control " />
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>UnitPrice:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="UnitPriceTextBox" runat="server" Text='<%# Bind("UnitPrice") %>' class="form-control " />
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>OnHand:</asp:TableCell><asp:TableCell>
                                                <asp:TextBox ID="OnHandTextBox" runat="server" Text='<%# Bind("OnHand") %>' class="form-control " />
                                            </asp:TableCell><asp:TableCell></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" ValidationGroup="New" />
                                            </asp:TableCell><asp:TableCell>
                                                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                                            </asp:TableCell></asp:TableRow></table></div></div></div></InsertItemTemplate><ItemTemplate>
                        <div style="" class="col-sm-6">
                            <div class="list-group">
                                <div class="list-group-item  bg-halloween">
                                    Current Product </div><div class="list-group-item">
                                    <table class="table ">
                                        <asp:TableRow>
                                            <asp:TableCell>                            
                                                ProductID:
                                            </asp:TableCell><asp:TableCell>
                                                <asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>' class="form-control " />
                                            </asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>
                                              Name:
                                            </asp:TableCell><asp:TableCell>
                                                <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' class="form-control " />
                                            </asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>ShortDescription:</asp:TableCell><asp:TableCell>
                                                <asp:Label ID="ShortDescriptionLabel" runat="server" Text='<%# Eval("ShortDescription") %>' class="form-control " />
                                            </asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>LongDescription:</asp:TableCell><asp:TableCell>
                                                <asp:Label ID="LongDescriptionLabel" runat="server" Text='<%# Eval("LongDescription") %>' class="form-control " />
                                            </asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>CategoryID:</asp:TableCell><asp:TableCell>
                                                <asp:Label ID="CategoryIDLabel" runat="server" Text='<%# Eval("CategoryID") %>' class="form-control " />
                                            </asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>ImageFile:</asp:TableCell><asp:TableCell>
                                                <asp:Label ID="ImageFileLabel" runat="server" Text='<%# Eval("ImageFile") %>' class="form-control " />
                                            </asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>UnitPrice:</asp:TableCell><asp:TableCell>
                                                <asp:Label ID="UnitPriceLabel" runat="server" Text='<%# Eval("UnitPrice","{0:f2}") %>' class="form-control " />
                                            </asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>OnHand:</asp:TableCell><asp:TableCell>
                                                <asp:Label ID="OnHandLabel" runat="server" Text='<%# Eval("OnHand") %>' class="form-control " />
                                            </asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit"/>
                                            </asp:TableCell><asp:TableCell>
                                                <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                                            </asp:TableCell></asp:TableRow></table></div></div></div></ItemTemplate><LayoutTemplate>
                        <div id="itemPlaceholderContainer" runat="server" style="" class="row">
                            <span runat="server" id="itemPlaceholder" />
                        </div>
                        <div style="" class="text-center">
                            <asp:DataPager ID="DataPager1" runat="server" PageSize="1">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Button" ButtonCssClass="btn btn-default" ShowFirstPageButton="True" ShowLastPageButton="True" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </LayoutTemplate>

                </asp:ListView>

                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Products] ORDER BY [ProductID]" DeleteCommand="DELETE FROM [Products] WHERE [ProductID] = @ProductID" InsertCommand="INSERT INTO [Products] ([ProductID], [Name], [ShortDescription], [LongDescription], [CategoryID], [ImageFile], [UnitPrice], [OnHand]) VALUES (@ProductID, @Name, @ShortDescription, @LongDescription, @CategoryID, @ImageFile, @UnitPrice, @OnHand)" UpdateCommand="UPDATE [Products] SET [Name] = @Name, [ShortDescription] = @ShortDescription, [LongDescription] = @LongDescription, [CategoryID] = @CategoryID, [ImageFile] = @ImageFile, [UnitPrice] = @UnitPrice, [OnHand] = @OnHand WHERE [ProductID] = @ProductID">
                    <DeleteParameters>
                        <asp:Parameter Name="ProductID" Type="String" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="ProductID" Type="String" />
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="ShortDescription" Type="String" />
                        <asp:Parameter Name="LongDescription" Type="String" />
                        <asp:Parameter Name="CategoryID" Type="String" />
                        <asp:Parameter Name="ImageFile" Type="String" />
                        <asp:Parameter Name="UnitPrice" Type="Decimal" />
                        <asp:Parameter Name="OnHand" Type="Int32" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="ShortDescription" Type="String" />
                        <asp:Parameter Name="LongDescription" Type="String" />
                        <asp:Parameter Name="CategoryID" Type="String" />
                        <asp:Parameter Name="ImageFile" Type="String" />
                        <asp:Parameter Name="UnitPrice" Type="Decimal" />
                        <asp:Parameter Name="OnHand" Type="Int32" />
                        <asp:Parameter Name="ProductID" Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource>

            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [CategoryID], [LongName] FROM [Categories] ORDER BY [LongName]"></asp:SqlDataSource></form>
        </main>
    </div>
</body>
</html>
