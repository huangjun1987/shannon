﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace XEx13CustomerList
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource2.SelectParameters.Clear();
            SqlDataSource2.SelectCommand = "SELECT LastName, FirstName, Email, State FROM Customers where State = @state ORDER BY LastName, FirstName";
            SqlDataSource2.SelectParameters.Add("state", System.Data.DbType.String, ddlState.SelectedValue);

            dlCustomers.DataSource = SqlDataSource2;
            //Binds the data to refresh every time it's used
            dlCustomers.DataBind();
        }
    }
}