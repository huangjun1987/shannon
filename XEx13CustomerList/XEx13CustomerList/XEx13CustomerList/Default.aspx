﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="XEx13CustomerList.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ch13: Customer List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/site.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        <header class="jumbotron"><%-- image set in site.css --%></header>
        <main>
            <form id="form1" runat="server" class="form-horizontal">
                <div class="form-group">
                    <label id="lblState" for="ddlState"
                        class="col-xs-4 col-sm-offset-1 col-sm-3 control-label">
                        Choose a state:</label>
                    <div class="col-xs-8 col-sm-5">
                        <asp:DropDownList ID="ddlState" CssClass="form-control" runat="server" DataSourceID="SqlDataSource1" DataTextField="StateName" DataValueField="StateCode" AutoPostBack="True"></asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HalloweenConnection %>" SelectCommand="SELECT [StateCode]
      ,[StateName]
  FROM [dbo].[States]
  order by [StateName]"></asp:SqlDataSource>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                        <asp:DataList ID="dlCustomers" runat="server" DataKeyField="Email" class="table table-bordered table-striped table-condensed">
                            <HeaderStyle CssClass="bg-halloween" />
                            <HeaderTemplate>
                                <span class="col-xs-3">LastName</span>
                                <span class="col-xs-3">FirstName</span>
                                <span class="col-xs-5">Email</span>
                                <span class="col-xs-1">State</span>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LastNameLabel" class="col-xs-3" runat="server" Text='<%# Eval("LastName") %>' />
                                <asp:Label ID="FirstNameLabel" class="col-xs-3" runat="server" Text='<%# Eval("FirstName") %>' />
                                <asp:Label ID="EmailLabel" class="col-xs-5" runat="server" Text='<%# Eval("Email") %>' />
                                <asp:Label ID="StateLabel" class="col-xs-1" runat="server" Text='<%# Eval("State") %>' />
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:HalloweenConnection %>" SelectCommand="SELECT LastName, FirstName, Email, State FROM Customers where State = @state ORDER BY LastName, FirstName"></asp:SqlDataSource>
                    </div>
                </div>

            </form>
        </main>
    </div>
</body>
</html>
