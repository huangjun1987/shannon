﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using XEx18ProductReceipt.Models;

namespace XEx18ProductReceipt
{
    public partial class Default : System.Web.UI.Page
    {
        private HalloweenEntities entities = new HalloweenEntities();
        public IQueryable<Product> Select([Control("ddlCategory")] string CategoryID)
        {
            if (string.IsNullOrWhiteSpace(CategoryID))
            {
                var defaultCategory = entities.Categories.OrderBy(c => c.LongName).FirstOrDefault();
                if (defaultCategory != null)
                    CategoryID = defaultCategory.CategoryID;
            }
            return entities.Products.Include("Category").Where(p => p.CategoryID.Equals(CategoryID)).OrderBy(p=>p.Name);

        }

        public IQueryable<Category> ddlCategory_GetData()
        {
            return entities.Categories.OrderBy(c => c.LongName);
        }

        public void Update(Product product)
        {
            var item = entities.Products.FirstOrDefault(p => p.ProductID.Equals(product.ProductID));
            if (ModelState.IsValid && item != null)
            {
                item.OnHand = product.OnHand;
                entities.SaveChanges();
            }
        }
    }
}