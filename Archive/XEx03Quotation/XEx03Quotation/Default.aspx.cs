﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace XEx03Quotation
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }
        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                int SalesPrice = Convert.ToInt32(txtSalesPrice.Text);
                int DiscountPercent = Convert.ToInt32(txtDiscountPercent.Text);

                decimal DiscountAmount = Convert.ToDecimal(SalesPrice * (DiscountPercent) / 100);

                decimal TotalPrice = Convert.ToDecimal(SalesPrice-DiscountAmount);

                lblDiscountAmount.Text = DiscountAmount.ToString("c");

                lblTotalPrice.Text = TotalPrice.ToString("c");
            }

        }

    }
}