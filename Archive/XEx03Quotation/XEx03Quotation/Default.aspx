﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="XEx03Quotation.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Price quotation</title>
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="Content/site.css" rel="stylesheet" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="Scripts/jquery-3.0.0.js"></script>
    <script src="Scripts/bootstrap.js"></script>

</head>
<body>
    <form id="form1" runat="server" class="form-horizontal">
        <main class="container">
            <h1 class="jumbotron">Price quotation</h1>

            <div class="form-group row">             
                    <div class="col-md-3">
                        Sales price
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtSalesPrice" runat="server" class="bold"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ErrorMessage="Required" ControlToValidate="txtSalesPrice" class="text-danger"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator1" runat="server"
                            ErrorMessage="Sales price must range from 10 and 1000" ControlToValidate="txtSalesPrice"
                            MaximumValue="1000" MinimumValue="10" Type="Integer"  class="text-danger"></asp:RangeValidator>
                    </div>
            </div>
            <div class="form-group row">
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
            </div>
            <div class="form-group row">
                <div class="col-md-3">Discount percent</div>
                <div class="col-md-3">
                    <asp:TextBox ID="txtDiscountPercent" runat="server" Font-Bold="False"
                        OnTextChanged="TextBox2_TextChanged"></asp:TextBox>
                </div>
                <div class="col-md-6 text-danger">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                        ErrorMessage="Required" ControlToValidate="txtDiscountPercent"  class="text-danger"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator2" runat="server"
                        ErrorMessage="Discount percent must range from 10 and 50"
                        ControlToValidate="txtDiscountPercent" MaximumValue="50" MinimumValue="10"
                        Type="Integer"  class="text-danger"></asp:RangeValidator>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3">Discount amount</div>
                <div class="col-md-3">
                    <asp:Label ID="lblDiscountAmount" runat="server" Text="" class="control-label bold"></asp:Label>
                </div>
                <div class="col-md-6">&nbsp;</div>
            </div>
            <div class="form-group row">
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
            </div>
            <div class="form-group row">
                <div class="col-md-3">Total price</div>
                <div class="col-md-3">
                    <asp:Label ID="lblTotalPrice" runat="server" Text="" class="control-label bold"></asp:Label>
                </div>
                <div class="col-md-6">&nbsp;</div>
            </div>
            <div class="form-group row">
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
            </div>
            <div class="form-group row">
                <div class="col-md-offset-3 col-md-3">
                    <asp:Button ID="btnCalculate" runat="server" Text="Calculate" OnClick="btnCalculate_Click" class="btn btn-primary"/>
                </div>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </div>
        </main>
    </form>
</body>
</html>
