﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="XEx02Quotation.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Price quotation</title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 114px;
        }
        .auto-style3 {
            width: 120px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Price quotation</h1>
    <table class="auto-style1">
        <tr>
            <td class="auto-style2">Sales price</td>
            <td class="auto-style3">
                <asp:TextBox ID="txtSalesPrice" runat="server" Font-Bold="True"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ErrorMessage="Required" ControlToValidate="txtSalesPrice"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" 
                    ErrorMessage="Sales price must range from 10 and 1000" ControlToValidate="txtSalesPrice" 
                    MaximumValue="1000" MinimumValue="10" Type="Integer"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">Discount percent</td>
            <td class="auto-style3">
                <asp:TextBox ID="txtDiscountPercent" runat="server" Font-Bold="False" 
                    OnTextChanged="TextBox2_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ErrorMessage="Required" ControlToValidate="txtDiscountPercent"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator2" runat="server" 
                    ErrorMessage="Discount percent must range from 10 and 50" 
                    ControlToValidate="txtDiscountPercent" MaximumValue="50" MinimumValue="10" 
                    Type="Integer"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Discount amount</td>
            <td class="auto-style3"><asp:Label ID="lblDiscountAmount" runat="server" Text="" Font-Bold="True"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">Total price</td>
            <td class="auto-style3">
                <asp:Label ID="lblTotalPrice" runat="server" Font-Bold="True" Text=""></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style3">&nbsp;</td> 
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Button ID="btnCalculate" runat="server" Text="Calculate" OnClick="btnCalculate_Click" />
            </td>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </form>
    </body>
</html>
