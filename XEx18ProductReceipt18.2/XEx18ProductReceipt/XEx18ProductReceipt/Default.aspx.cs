﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using XEx18ProductReceipt.Models;

namespace XEx18ProductReceipt
{
    public partial class Default : System.Web.UI.Page
    {

        protected void ddlCategory_CallingDataMethods(object sender, CallingDataMethodsEventArgs e)
        {
            e.DataMethodsObject = new ProductDB();
        }
    }
}