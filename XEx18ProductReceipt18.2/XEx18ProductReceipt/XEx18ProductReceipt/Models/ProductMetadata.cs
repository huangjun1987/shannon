﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace XEx18ProductReceipt.Models
{
    public class ProductMetadata
    {
        [Range(1,500, ErrorMessage = "Please enter an integer between 1 and 500")]
        public int OnHand { get; set; }

    }
    [MetadataType(typeof(ProductMetadata))]
    partial class Product
    {

    }
}