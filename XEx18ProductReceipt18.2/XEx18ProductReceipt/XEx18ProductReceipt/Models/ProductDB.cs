﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI.WebControls;
namespace XEx18ProductReceipt.Models
{
    public class ProductDB
    {
        private HalloweenEntities db;

        public ProductDB()
        {
            db = new HalloweenEntities();
        }

        public IQueryable<Category> GetCategories()
        {
            return db.Categories.OrderBy(c => c.LongName);
        }
        public IQueryable<Product> GetProductsByCategory([Control("ddlCategory")] string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                var defaultCategory = db.Categories.OrderBy(c => c.LongName).FirstOrDefault();
                if (defaultCategory != null)
                    id = defaultCategory.CategoryID;
            }
            return db.Products.Include("Category").Where(p => p.CategoryID.Equals(id)).OrderBy(p => p.Name);

        }

        public void UpdateProduct(ModelMethodContext modelMethodContext, Product product)
        {
            var item = db.Products.FirstOrDefault(p => p.ProductID.Equals(product.ProductID));
            if (modelMethodContext.ModelState.IsValid && item != null)
            {
                item.OnHand = product.OnHand;
                db.SaveChanges();
            }
        }
    }
}